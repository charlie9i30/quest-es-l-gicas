<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 30/08/2019
 * Time: 19:31
 */

class circle{
    private $area;
    private $perimetro;

    public function __construct($area,$perimetro){
        $this->area = $area;
        $this->perimetro = $perimetro;
    }

    public function pegarArea(){
        $this->area = pi()*$this->perimetro*$this->perimetro;
        return $this->area;
    }
    public function pegarperimetro(){
        $this->perimetro = 2*pi()*$this->perimetro;
        return $this->perimetro;
    }
}
function pegarArea(){
    $area = new circle(50,50);
    $area->pegarArea();
    var_dump($area);

}
function pegarperimetro(){
    $perimetro = new circle(50,50);
    $perimetro->pegarperimetro();
    var_dump($perimetro);
}

pegarperimetro();
pegarArea();
?>