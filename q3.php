<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 30/08/2019
 * Time: 19:30
 */

$frase = "o rato roeu a roupa do rei de roma"; // frase em minusculo
$ordenada;
$delimitador = 3;

$menor = array_filter(explode(' ', $frase), function($val){
    return strlen($val) < 3;
});

$menorespalavras = implode (' ',$menor);
$ordenada = preg_split('/ /',$menorespalavras,-1,PREG_SPLIT_OFFSET_CAPTURE);
sort($ordenada);

print_r($ordenada);

?>